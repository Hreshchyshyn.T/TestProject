package com.t.hreshchyshyn.testapp;

import android.app.Application;

import com.t.hreshchyshyn.testapp.main.persistence.UsersDatabase;

public class TestApplication extends Application {
    public static UsersDatabase usersDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        usersDatabase = UsersDatabase.getInstance(this);
    }
}
