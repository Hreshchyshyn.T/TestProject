package com.t.hreshchyshyn.testapp.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.t.hreshchyshyn.testapp.common.Constants;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitUtils {

    private static final String TAG = RetrofitUtils.class.getSimpleName();
    private static OkHttpClient.Builder okHttpClient = null;

    public static RetrofitApi getApiServiceRx() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(createHttpClient().build())
                .build()
                .create(RetrofitApi.class);
    }

    private static OkHttpClient.Builder createHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(logging);
        }

        return okHttpClient;
    }
}
