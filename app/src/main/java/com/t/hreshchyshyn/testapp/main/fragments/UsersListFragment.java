package com.t.hreshchyshyn.testapp.main.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.main.UsersAdapter;
import com.t.hreshchyshyn.testapp.main.interfaces.IUsersListPresenter;
import com.t.hreshchyshyn.testapp.main.models.UserModel;
import com.t.hreshchyshyn.testapp.main.presenters.UsersListPresenter;
import com.t.hreshchyshyn.testapp.main.views.UsersListView;
import com.t.hreshchyshyn.testapp.mvp.BaseFragment;

import java.util.List;

public class UsersListFragment extends BaseFragment<IUsersListPresenter>
        implements UsersListView, UsersAdapter.OnUserClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rvUsers;
    private UsersAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    public UsersListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users_list, container, false);
        presenter = new UsersListPresenter(this);
        initUI(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new UsersAdapter(this);
        setupUI();
        presenter.getUsers();
    }

    private void initUI(View view){
        rvUsers = view.findViewById(R.id.rv_users);
        refreshLayout = view.findViewById(R.id.swipe_refresh_layout);
    }

    private void setupUI(){
        rvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvUsers.setAdapter(adapter);

        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void setupUsers(List<UserModel> users)    {
        adapter.setUsers(users);
    }

    private void stopRefreshingLayout(){
        if (refreshLayout.isRefreshing())
            refreshLayout.setRefreshing(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        stopRefreshingLayout();
    }

    @Override
    public void onClick(UserModel model) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, UserDetailsFragment.newInstance(model))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRefresh() {
        presenter.getUsers();
    }
}
