package com.t.hreshchyshyn.testapp.main.interfaces;

public interface IMainPresenter {
    void performLogout();
    void getCurrentUser();
}
