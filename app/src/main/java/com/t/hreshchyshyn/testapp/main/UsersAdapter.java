package com.t.hreshchyshyn.testapp.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.main.models.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private List<UserModel> items;
    private OnUserClickListener listener;

    public UsersAdapter(OnUserClickListener listener) {
        items = new ArrayList<>();
        this.listener = listener;
    }

    public void setUsers(List<UserModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvUsername, tvPhone,
                tvEmail, tvCity, tvCompany;

        private View root;

        ViewHolder(View view) {
            super(view);
            root = view.findViewById(R.id.root_view);
            tvName = view.findViewById(R.id.tv_name);
            tvUsername = view.findViewById(R.id.tv_username);
            tvPhone = view.findViewById(R.id.tv_phone);
            tvEmail = view.findViewById(R.id.tv_email);
            tvCity = view.findViewById(R.id.tv_city);
            tvCompany = view.findViewById(R.id.tv_company);
        }

        void bind(final UserModel model) {
            tvName.setText(model.getName());
            tvUsername.setText(model.getUsername());
            tvPhone.setText(model.getPhone());
            tvEmail.setText(model.getEmail());
            tvCity.setText(model.getAddress().getCity());
            tvCompany.setText(model.getCompany().getCompanyName());

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(model);
                }
            });
        }
    }

    public interface OnUserClickListener {
        void onClick(UserModel model);
    }
}
