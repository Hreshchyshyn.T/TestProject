package com.t.hreshchyshyn.testapp.main.persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.t.hreshchyshyn.testapp.main.models.UserModel;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface UsersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsers(List<UserModel> models);

    @Query("SELECT * FROM Users")
    Single<List<UserModel>> getAllUsers();

    @Query("DELETE FROM Users")
    void clearTable();
}
