package com.t.hreshchyshyn.testapp.main.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CompanyModel implements Parcelable{
    @SerializedName("name")
    private String companyName;
    private String catchPhrase;
    private String bs;

    public CompanyModel(String companyName, String catchPhrase, String bs) {
        this.companyName = companyName;
        this.catchPhrase = catchPhrase;
        this.bs = bs;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public String getBs() {
        return bs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompanyModel that = (CompanyModel) o;

        if (companyName != null ? !companyName.equals(that.companyName) : that.companyName != null) return false;
        if (catchPhrase != null ? !catchPhrase.equals(that.catchPhrase) : that.catchPhrase != null)
            return false;
        return bs != null ? bs.equals(that.bs) : that.bs == null;
    }

    @Override
    public int hashCode() {
        int result = companyName != null ? companyName.hashCode() : 0;
        result = 31 * result + (catchPhrase != null ? catchPhrase.hashCode() : 0);
        result = 31 * result + (bs != null ? bs.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(companyName);
        parcel.writeString(catchPhrase);
        parcel.writeString(bs);
    }

    private CompanyModel(Parcel in) {
        companyName = in.readString();
        catchPhrase = in.readString();
        bs = in.readString();
    }

    public static final Creator<CompanyModel> CREATOR = new Creator<CompanyModel>() {
        @Override
        public CompanyModel createFromParcel(Parcel in) {
            return new CompanyModel(in);
        }

        @Override
        public CompanyModel[] newArray(int size) {
            return new CompanyModel[size];
        }
    };
}
