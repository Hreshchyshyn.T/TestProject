package com.t.hreshchyshyn.testapp.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.common.Constants;
import com.t.hreshchyshyn.testapp.common.FbConstants;
import com.t.hreshchyshyn.testapp.common.PrefManager;
import com.t.hreshchyshyn.testapp.common.Utils;
import com.t.hreshchyshyn.testapp.login.models.CurrentUserModel;
import com.t.hreshchyshyn.testapp.mvp.BasePresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

public class LoginPresenter extends BasePresenter<LoginView>
        implements ILoginPresenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();
    private PrefManager prefManager;

    LoginPresenter(LoginView view) {
        setView(view);
        prefManager = new PrefManager(view.getContext());
    }

    @Override
    public boolean needToStartMainActivity() {
        return isUserLoggedIn() && !isEmailPermissionsDeclined();
    }

    private boolean isUserLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired();
    }

    private void saveUser(String email) {
        Profile profile = Profile.getCurrentProfile();
        String name = profile.getName();
        String pictureUrl = profile.getProfilePictureUri(480, 480).toString();
        CurrentUserModel model = new CurrentUserModel(name, email, pictureUrl);
        prefManager.saveStringUserData(Constants.CURRENT_USER, new Gson().toJson(model));
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Log.i(TAG, "onSuccess");
        getUserEmail(loginResult);
    }

    private void getUserEmail(final LoginResult loginResult) {
        view.showLoading();
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        view.hideLoading();
                        if (isEmailPermissionsDeclined())
                            view.showEmailPermissionsDeniedDialog();
                        else {
                            try {
                                String email = object.getString(FbConstants.PERMISSIONS_EMAIL);
                                saveUser(email);
                                view.startMainActivity();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

        Bundle params = new Bundle();
        params.putString(FbConstants.FIELDS, FbConstants.PERMISSIONS_EMAIL);
        request.setParameters(params);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
        Log.i(TAG, "onCancel");
        if (isUserLoggedIn() && isEmailPermissionsDeclined())
            view.showEmailPermissionsDeniedDialog();
    }

    @Override
    public void onError(FacebookException error) {
        Log.i(TAG, "onError: ");
        if (!Utils.isConnectionAvailable(view.getContext()))
            view.showErrorSnackBar(getString(R.string.error_text_internet));
    }

    @Override
    public void askEmailPermissions() {
        LoginManager.getInstance().logInWithReadPermissions(
                ((AppCompatActivity) view.getContext()),
                Collections.singletonList(FbConstants.PERMISSIONS_EMAIL)
        );
    }

    @Override
    public boolean isEmailPermissionsDeclined() {
        return AccessToken.getCurrentAccessToken() != null
                && AccessToken.getCurrentAccessToken().getDeclinedPermissions().contains(FbConstants.PERMISSIONS_EMAIL);
    }
}
