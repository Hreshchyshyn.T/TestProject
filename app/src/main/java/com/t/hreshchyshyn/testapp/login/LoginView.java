package com.t.hreshchyshyn.testapp.login;

import com.t.hreshchyshyn.testapp.mvp.BaseView;

public interface LoginView extends BaseView {
    void startMainActivity();
    void showEmailPermissionsDeniedDialog();
}
