package com.t.hreshchyshyn.testapp.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

    private static final String PREF_NAME = "TEST_APP_PREF";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public PrefManager(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void saveStringUserData(String key, String value){
        editor.putString(key, value);
        editor.commit();
    }

    public String  getStringUserData(String key){
        return preferences.getString(key, "");
    }
}
