package com.t.hreshchyshyn.testapp.network;

import com.t.hreshchyshyn.testapp.main.models.UserModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RetrofitApi {
    @GET("users")
    Observable<List<UserModel>> getUsers();
}
