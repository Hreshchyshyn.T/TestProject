package com.t.hreshchyshyn.testapp.main.views;

import com.t.hreshchyshyn.testapp.main.models.UserModel;
import com.t.hreshchyshyn.testapp.mvp.BaseView;

import java.util.List;

public interface UsersListView extends BaseView {
    void setupUsers(List<UserModel> users);
}
