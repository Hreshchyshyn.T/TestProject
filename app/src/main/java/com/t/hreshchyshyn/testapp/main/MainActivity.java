package com.t.hreshchyshyn.testapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.login.LoginActivity;
import com.t.hreshchyshyn.testapp.login.models.CurrentUserModel;
import com.t.hreshchyshyn.testapp.main.fragments.UsersListFragment;
import com.t.hreshchyshyn.testapp.main.interfaces.IMainPresenter;
import com.t.hreshchyshyn.testapp.main.presenters.MainPresenter;
import com.t.hreshchyshyn.testapp.main.views.MainView;
import com.t.hreshchyshyn.testapp.mvp.BaseActivity;
import com.t.hreshchyshyn.testapp.mvp.BaseView;

public class MainActivity extends BaseActivity
        implements BaseView, NavigationView.OnNavigationItemSelectedListener, MainView {

    private View pbContainer;
    private DrawerLayout drawer;
    private IMainPresenter presenter;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        initUI();
        setupToolbar();
        setupUI();
        presenter.getCurrentUser();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new UsersListFragment())
                .commit();

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setTitle("");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawer.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupUI() {
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initUI() {
        pbContainer = findViewById(R.id.pb_container);
        drawer = findViewById(R.id.drawer_layout);
    }

    @Override
    public void showLoading() {
        pbContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbContainer.setVisibility(View.GONE);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                presenter.performLogout();
                break;
            case R.id.close_app:
                finish();
                break;
        }
        return false;
    }

    @Override
    public void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void setupDrawerHeader(CurrentUserModel model) {
        View headerView = navigationView.getHeaderView(0);
        ImageView userImage = headerView.findViewById(R.id.ic_user_image);
        TextView userName = headerView.findViewById(R.id.tv_user_name);
        TextView userEmail = headerView.findViewById(R.id.tv_email);

        Glide.with(this)
                .load(model.getPhotoUrl())
                .into(userImage);

        userEmail.setText(model.getEmail());
        userName.setText(model.getName());
    }
}
