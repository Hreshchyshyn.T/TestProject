package com.t.hreshchyshyn.testapp.main.interfaces;

import com.t.hreshchyshyn.testapp.mvp.IBasePresenter;

public interface IUsersListPresenter extends IBasePresenter {
    void getUsers();
}
