package com.t.hreshchyshyn.testapp.main.presenters;

import com.t.hreshchyshyn.testapp.TestApplication;
import com.t.hreshchyshyn.testapp.main.UsersListService;
import com.t.hreshchyshyn.testapp.main.UsersStorage;
import com.t.hreshchyshyn.testapp.main.interfaces.IUsersListPresenter;
import com.t.hreshchyshyn.testapp.main.models.UserModel;
import com.t.hreshchyshyn.testapp.main.views.UsersListView;
import com.t.hreshchyshyn.testapp.mvp.BasePresenter;
import com.t.hreshchyshyn.testapp.network.RetrofitException;

import java.util.List;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class UsersListPresenter extends BasePresenter<UsersListView>
        implements IUsersListPresenter {

    private UsersListService service;
    private UsersStorage storage;

    public UsersListPresenter(UsersListView view) {
        setView(view);
        storage = new UsersStorage(TestApplication.usersDatabase.usersDao());
        service = new UsersListService();
    }

    @Override
    public void getUsers() {
        view.showLoading();
        service.getUsers()
                .doOnNext(new Consumer<List<UserModel>>() {
                    @Override
                    public void accept(List<UserModel> models) {
                        storage.saveUsers(models);
                    }
                })
                .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends List<UserModel>>>() {
                    @Override
                    public ObservableSource<? extends List<UserModel>> apply(Throwable throwable) {
                        if (throwable instanceof RetrofitException
                                && ((RetrofitException) throwable).getKind() == RetrofitException.Kind.NETWORK) {
                            handleRetrofitException((RetrofitException) throwable);
                            return storage.getUsersFromStorage();
                        }
                        return io.reactivex.Observable.error(throwable);
                    }
                })
                .subscribe(new Observer<List<UserModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<UserModel> userModels) {
                        view.setupUsers(userModels);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoading();
                        e.printStackTrace();
                        if (e instanceof RetrofitException) {
                            handleRetrofitException(((RetrofitException) e));
                        }
                    }

                    @Override
                    public void onComplete() {
                        view.hideLoading();
                    }
                });
    }
}
