package com.t.hreshchyshyn.testapp.main.models;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoModel implements Parcelable{
    private double lat;
    private double lng;

    public GeoModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoModel geoModel = (GeoModel) o;

        if (Double.compare(geoModel.lat, lat) != 0) return false;
        return Double.compare(geoModel.lng, lng) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
    }

    private GeoModel(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public static final Creator<GeoModel> CREATOR = new Creator<GeoModel>() {
        @Override
        public GeoModel createFromParcel(Parcel in) {
            return new GeoModel(in);
        }

        @Override
        public GeoModel[] newArray(int size) {
            return new GeoModel[size];
        }
    };
}
