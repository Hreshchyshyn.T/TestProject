package com.t.hreshchyshyn.testapp.common;

public interface Constants {
    String CURRENT_USER = "CURRENT_USER";
    String BASE_URL = "https://jsonplaceholder.typicode.com/";
}
