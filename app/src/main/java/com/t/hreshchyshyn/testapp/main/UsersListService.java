package com.t.hreshchyshyn.testapp.main;

import com.t.hreshchyshyn.testapp.main.models.UserModel;
import com.t.hreshchyshyn.testapp.network.RetrofitApi;
import com.t.hreshchyshyn.testapp.network.RetrofitUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class UsersListService {
    private RetrofitApi api = RetrofitUtils.getApiServiceRx();

    public Observable<List<UserModel>> getUsers(){
        return api.getUsers()
                .observeOn(AndroidSchedulers.mainThread());
    }
}
