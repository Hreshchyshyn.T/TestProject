package com.t.hreshchyshyn.testapp.main.models;

import android.arch.persistence.room.Embedded;
import android.os.Parcel;
import android.os.Parcelable;

public class AddressModel implements Parcelable {
    private String street;
    private String suite;
    private String city;
    private String zipcode;
    @Embedded
    private GeoModel geo;

    public AddressModel(String street, String suite, String city, String zipcode, GeoModel geo) {
        this.street = street;
        this.suite = suite;
        this.city = city;
        this.zipcode = zipcode;
        this.geo = geo;
    }

    public String getStreet() {
        return street;
    }

    public String getSuite() {
        return suite;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public GeoModel getGeo() {
        return geo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressModel that = (AddressModel) o;

        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (suite != null ? !suite.equals(that.suite) : that.suite != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (zipcode != null ? !zipcode.equals(that.zipcode) : that.zipcode != null) return false;
        return geo != null ? geo.equals(that.geo) : that.geo == null;
    }

    @Override
    public int hashCode() {
        int result = street != null ? street.hashCode() : 0;
        result = 31 * result + (suite != null ? suite.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (zipcode != null ? zipcode.hashCode() : 0);
        result = 31 * result + (geo != null ? geo.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(street);
        parcel.writeString(suite);
        parcel.writeString(city);
        parcel.writeString(zipcode);
        parcel.writeParcelable(geo, i);
    }

    private AddressModel(Parcel in) {
        street = in.readString();
        suite = in.readString();
        city = in.readString();
        zipcode = in.readString();
        geo = in.readParcelable(GeoModel.class.getClassLoader());
    }

    public static final Creator<AddressModel> CREATOR = new Creator<AddressModel>() {
        @Override
        public AddressModel createFromParcel(Parcel in) {
            return new AddressModel(in);
        }

        @Override
        public AddressModel[] newArray(int size) {
            return new AddressModel[size];
        }
    };
}
