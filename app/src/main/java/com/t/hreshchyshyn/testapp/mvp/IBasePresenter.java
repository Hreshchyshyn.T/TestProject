package com.t.hreshchyshyn.testapp.mvp;

public interface IBasePresenter {
    void onAttach();

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();
}
