package com.t.hreshchyshyn.testapp.login;

import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.t.hreshchyshyn.testapp.mvp.IBasePresenter;

public interface ILoginPresenter extends IBasePresenter, FacebookCallback<LoginResult> {
    boolean needToStartMainActivity();
    void askEmailPermissions();
    boolean isEmailPermissionsDeclined();
}
