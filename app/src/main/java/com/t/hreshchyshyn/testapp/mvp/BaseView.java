package com.t.hreshchyshyn.testapp.mvp;

import android.content.Context;

public interface BaseView {
    void showSnackBar(String msg);
    void showErrorSnackBar(String msg);
    Context getContext();
    void showLoading();
    void hideLoading();
}
