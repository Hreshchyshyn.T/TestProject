package com.t.hreshchyshyn.testapp.mvp;

import android.support.annotation.StringRes;

import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.network.RetrofitException;

public abstract class BasePresenter<V extends BaseView> implements IBasePresenter {

    protected V view;

    protected void setView(V view) {
        this.view = view;
    }

    protected String getString(@StringRes int msgId) {
        return view.getContext().getString(msgId);
    }

    protected void handleRetrofitException(RetrofitException e) {
        switch (e.getKind()) {
            case NETWORK:
                view.showErrorSnackBar(getString(R.string.error_text_internet));
                break;
            case HTTP:
                view.showErrorSnackBar(getString(R.string.error_text_http));
                break;
            case UNEXPECTED:
                view.showErrorSnackBar(getString(R.string.error_text_unexpected));
                break;
        }
    }

    @Override
    public void onAttach() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
