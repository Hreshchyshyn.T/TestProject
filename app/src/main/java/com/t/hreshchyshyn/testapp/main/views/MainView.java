package com.t.hreshchyshyn.testapp.main.views;

import com.t.hreshchyshyn.testapp.login.models.CurrentUserModel;
import com.t.hreshchyshyn.testapp.mvp.BaseView;

public interface MainView extends BaseView {
    void startLoginActivity();
    void setupDrawerHeader(CurrentUserModel model);
}
