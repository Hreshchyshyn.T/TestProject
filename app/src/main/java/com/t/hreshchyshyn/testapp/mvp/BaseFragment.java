package com.t.hreshchyshyn.testapp.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class BaseFragment<T extends IBasePresenter> extends Fragment implements BaseView {

    protected T presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (presenter != null) this.presenter.onAttach();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (presenter != null) this.presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null) this.presenter.onResume();
    }

    @Override
    public void onPause() {
        if (presenter != null) this.presenter.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        if (presenter != null) this.presenter.onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            this.presenter.onDestroy();
            this.presenter = null;
        }
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        if (getActivity() != null)
            ((BaseView) getActivity()).showLoading();
    }

    @Override
    public void hideLoading() {
        if (getActivity() != null) {
            ((BaseView) getActivity()).hideLoading();
        }
    }

    @Override
    public void showSnackBar(String message) {
        if (getActivity() != null) {
            ((BaseView) getActivity()).showSnackBar(message);
        }
    }

    @Override
    public void showErrorSnackBar(String message) {
        if (getActivity() != null) {
            ((BaseView) getActivity()).showErrorSnackBar(message);
        }
    }
}
