package com.t.hreshchyshyn.testapp.mvp;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.widget.TextView;

import com.t.hreshchyshyn.testapp.R;

public abstract class BaseActivity extends AppCompatActivity implements BaseView{
    @Override
    public void showSnackBar(String msg) {
        createSnackBar(ContextCompat.getColor(this, R.color.colorPrimary), msg);
    }

    @Override
    public void showErrorSnackBar(String msg) {
        if (!msg.isEmpty()) {
            createSnackBar(ContextCompat.getColor(this, R.color.colorRed), msg);
        }

    }

    protected void createSnackBar(int color, String message) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content_full_screen), message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(color);
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.snackbar_textsize));
        textView.setMaxLines(4);
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        snackbar.show();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
