package com.t.hreshchyshyn.testapp.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.common.FbConstants;
import com.t.hreshchyshyn.testapp.main.MainActivity;
import com.t.hreshchyshyn.testapp.mvp.BaseActivity;
import com.t.hreshchyshyn.testapp.mvp.BaseView;

import java.util.Arrays;

public class LoginActivity extends BaseActivity
        implements BaseView, LoginView {

    private CallbackManager callbackManager;
    private ILoginPresenter presenter;
    private View pbContainer;
    private LoginButton fbLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(this);
        callbackManager = CallbackManager.Factory.create();
        checkForLoginAndPermissions();

        initUI();
        setupUI();
    }

    private void checkForLoginAndPermissions() {
        if (presenter.needToStartMainActivity())
            startMainActivity();
        else if (presenter.isEmailPermissionsDeclined())
            showEmailPermissionsDeniedDialog();
    }

    private void initUI() {
        fbLoginButton = findViewById(R.id.btn_fb_login);
        pbContainer = findViewById(R.id.pb_container);
    }

    private void setupUI() {
        fbLoginButton.setReadPermissions(Arrays.asList(FbConstants.PERMISSIONS_EMAIL, FbConstants.PERMISSIONS_PUBLIC_PROFILE));
        fbLoginButton.registerCallback(callbackManager, presenter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        pbContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbContainer.setVisibility(View.GONE);
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showEmailPermissionsDeniedDialog(){
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(R.string.text_grant_email_permissions)
                .setTitle(R.string.error_text)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.askEmailPermissions();
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }
}
