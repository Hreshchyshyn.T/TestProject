package com.t.hreshchyshyn.testapp.main;

import com.t.hreshchyshyn.testapp.main.models.UserModel;
import com.t.hreshchyshyn.testapp.main.persistence.UsersDao;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class UsersStorage {
    private UsersDao usersDao;

    public UsersStorage(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    public void saveUsers(final List<UserModel> models) {
        Completable.fromAction(new Action() {
            @Override
            public void run() {
                usersDao.insertUsers(models);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public Observable<List<UserModel>> getUsersFromStorage() {
        return usersDao.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable();
    }
}
