package com.t.hreshchyshyn.testapp.main.presenters;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.t.hreshchyshyn.testapp.common.Constants;
import com.t.hreshchyshyn.testapp.common.PrefManager;
import com.t.hreshchyshyn.testapp.login.models.CurrentUserModel;
import com.t.hreshchyshyn.testapp.main.interfaces.IMainPresenter;
import com.t.hreshchyshyn.testapp.main.views.MainView;
import com.t.hreshchyshyn.testapp.mvp.BasePresenter;

public class MainPresenter extends BasePresenter<MainView> implements IMainPresenter {
    private PrefManager prefManager;
    public MainPresenter(MainView view){
        setView(view);
        prefManager = new PrefManager(view.getContext());
    }


    @Override
    public void performLogout() {
        LoginManager.getInstance().logOut();
        view.startLoginActivity();
    }

    @Override
    public void getCurrentUser() {
        String userJson = prefManager.getStringUserData(Constants.CURRENT_USER);
        CurrentUserModel model = new Gson().fromJson(userJson, CurrentUserModel.class);
        view.setupDrawerHeader(model);
    }
}
