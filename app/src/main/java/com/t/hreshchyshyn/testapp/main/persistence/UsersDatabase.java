package com.t.hreshchyshyn.testapp.main.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.t.hreshchyshyn.testapp.main.models.UserModel;

@Database(entities = {UserModel.class}, version = 1)
public abstract class UsersDatabase extends RoomDatabase {
    public abstract UsersDao usersDao();

    private static volatile UsersDatabase INSTANCE = null;

    public static UsersDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (UsersDatabase.class) {
                INSTANCE = buildDatabase(context);
            }
        }

        return INSTANCE;
    }

    private static UsersDatabase buildDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                UsersDatabase.class, "Users")
                .fallbackToDestructiveMigration()
                .build();
    }
}
