package com.t.hreshchyshyn.testapp.common;

public interface FbConstants {
    String PERMISSIONS_EMAIL = "email";
    String PERMISSIONS_PUBLIC_PROFILE = "public_profile";
    String FIELDS = "fields";
}
