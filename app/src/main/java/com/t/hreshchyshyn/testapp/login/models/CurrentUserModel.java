package com.t.hreshchyshyn.testapp.login.models;

public class CurrentUserModel {
    private String name;
    private String email;
    private String photoUrl;

    public CurrentUserModel(String name, String email, String photoUrl){
        this.photoUrl = photoUrl;
        this.name = name;
        this.email = email;
    }

    public CurrentUserModel(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
