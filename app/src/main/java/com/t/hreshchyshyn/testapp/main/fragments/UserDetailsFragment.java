package com.t.hreshchyshyn.testapp.main.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.t.hreshchyshyn.testapp.R;
import com.t.hreshchyshyn.testapp.main.models.GeoModel;
import com.t.hreshchyshyn.testapp.main.models.UserModel;

public class UserDetailsFragment extends Fragment implements OnMapReadyCallback {

    private static final String USER_MODEL = "USER_MODEL";

    private UserModel userModel;
    private MapView mapView;
    private TextView tvName, tvEmail, tvUsername, tvPhone, tvWebsite,
            tvCity, tvStreet, tvSuite, tvZipcode, tvCompanyName, tvSlogan, tvBusiness;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    public static UserDetailsFragment newInstance(UserModel userModel) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER_MODEL, userModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        readArgs(getArguments());
        initUI(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initMapView(view, savedInstanceState);
        setupUI();
        makeMapViewScrollable(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mapView != null)
            mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null)
            mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null)
            mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mapView != null)
            mapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void makeMapViewScrollable(View view) {
        final NestedScrollView nestedScrollView = view.findViewById(R.id.nested_scroll_view);
        ImageView transparent = view.findViewById(R.id.transparent);

        transparent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        nestedScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        nestedScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        nestedScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    private void setupUI() {
        tvName.setText(userModel.getName());
        tvEmail.setText(userModel.getEmail());
        tvUsername.setText(userModel.getUsername());
        tvPhone.setText(userModel.getPhone());
        tvWebsite.setText(userModel.getWebsite());
        tvCity.setText(userModel.getAddress().getCity());
        tvStreet.setText(userModel.getAddress().getStreet());
        tvSuite.setText(userModel.getAddress().getSuite());
        tvZipcode.setText(userModel.getAddress().getZipcode());
        tvCompanyName.setText(userModel.getCompany().getCompanyName());
        tvSlogan.setText(userModel.getCompany().getCatchPhrase());
        tvBusiness.setText(userModel.getCompany().getBs());


    }


    private void initMapView(View view, Bundle savedInstanceState) {
        mapView = view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);

        if (getActivity() != null) {
            mapView.getMapAsync(this);
        }
    }

    private void initUI(View view) {
        tvName = view.findViewById(R.id.tv_name);
        tvEmail = view.findViewById(R.id.tv_email);
        tvUsername = view.findViewById(R.id.tv_username);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvWebsite = view.findViewById(R.id.tv_website);
        tvCity = view.findViewById(R.id.tv_city);
        tvStreet = view.findViewById(R.id.tv_street);
        tvSuite = view.findViewById(R.id.tv_suite);
        tvZipcode = view.findViewById(R.id.tv_zipcode);
        tvCompanyName = view.findViewById(R.id.tv_company_name);
        tvSlogan = view.findViewById(R.id.tv_phrase);
        tvBusiness = view.findViewById(R.id.tv_bs);
    }

    private void readArgs(Bundle args) {
        if (args == null) {
            throw new IllegalStateException(
                    "Arguments == null!! Please, use UserDetailsFragment.newInstance() to create and use "
                            + UserDetailsFragment.class.getSimpleName()
            );
        }
        userModel = args.getParcelable(USER_MODEL);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GeoModel geo = userModel.getAddress().getGeo();
        LatLng marker = new LatLng(geo.getLat(), geo.getLng());
        MarkerOptions options = new MarkerOptions()
                .position(marker)
                .title(userModel.getName());

        googleMap.addMarker(options);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(marker).zoom(10f).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
