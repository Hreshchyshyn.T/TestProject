package com.t.hreshchyshyn.testapp.main.models;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "Users")
public class UserModel implements Parcelable {
    @PrimaryKey
    private int id;
    private String name;
    private String email;
    private String username;
    @Embedded
    private AddressModel address;
    private String phone;
    private String website;
    @Embedded
    private CompanyModel company;

    public UserModel(int id, String name, String email, String username,
                     AddressModel address, String phone, String website, CompanyModel company) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.username = username;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public AddressModel getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public CompanyModel getCompany() {
        return company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserModel userModel = (UserModel) o;

        if (id != userModel.id) return false;
        if (name != null ? !name.equals(userModel.name) : userModel.name != null) return false;
        if (email != null ? !email.equals(userModel.email) : userModel.email != null) return false;
        if (username != null ? !username.equals(userModel.username) : userModel.username != null)
            return false;
        if (address != null ? !address.equals(userModel.address) : userModel.address != null)
            return false;
        if (phone != null ? !phone.equals(userModel.phone) : userModel.phone != null) return false;
        if (website != null ? !website.equals(userModel.website) : userModel.website != null)
            return false;
        return company != null ? company.equals(userModel.company) : userModel.company == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(username);
        parcel.writeParcelable(address, i);
        parcel.writeString(phone);
        parcel.writeString(website);
        parcel.writeParcelable(company, i);
    }

    private UserModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        email = in.readString();
        username = in.readString();
        address = in.readParcelable(AddressModel.class.getClassLoader());
        phone = in.readString();
        website = in.readString();
        company = in.readParcelable(CompanyModel.class.getClassLoader());
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
